Parts, Queue, Queue2, Queue4, Queue8, Queue16, Queue32 = list(), list(), list(), list(), list(), list(), list()
Memory = [2, 0, 2, 0, 4, 0, 4, 0, 8, 0, 8, 0, 16, 0, 16, 0, 32, 0, 32, 0]
Time, Count = 0.0, 0


def clear():
    global Parts, Queue, Queue2, Queue4, Queue8, Queue16, Queue32, Memory, Time, Count
    Parts, Queue, Queue2, Queue4, Queue8, Queue16, Queue32 = list(), list(), list(), list(), list(), list(), list()
    Memory = [2, 0, 2, 0, 4, 0, 4, 0, 8, 0, 8, 0, 16, 0, 16, 0, 32, 0, 32, 0]
    Time, Count = 0.0, 0


def where(data):
    global Memory
    if data == 2:
        if Memory[1] == 0:
            return 1
        elif Memory[3] == 0:
            return 3
    elif data == 4:
        if Memory[5] == 0:
            return 5
        elif Memory[7] == 0:
            return 7
    elif data == 8:
        if Memory[9] == 0:
            return 9
        elif Memory[11] == 0:
            return 11
    elif data == 16:
        if Memory[13] == 0:
            return 13
        elif Memory[15] == 0:
            return 15
    elif data == 32:
        if Memory[17] == 0:
            return 17
        elif Memory[19] == 0:
            return 19
    else:
        return -1


def enter(mq):
    global Parts, Queue, Queue2, Queue4, Queue8, Queue16, Queue32, Memory, Time, Count
    if mq:
        if len(Queue2) != 0:
            data = Queue2.pop()
            location = where(data)
            if location == -1:
                Queue2.insert(0, data)
            else:
                try:
                    Memory[location] = 0
                    Time += int(Parts[location]['time'])
                except:
                    pass
        if len(Queue4) != 0:
            data = Queue4.pop()
            location = where(data)
            if location == -1:
                Queue4.insert(0, data)
            else:
                try:
                    Memory[location] = 0
                    Time += int(Parts[location]['time'])
                except:
                    pass
        if len(Queue8) != 0:
            data = Queue8.pop()
            location = where(data)
            if location == -1:
                Queue8.insert(0, data)
            else:
                try:
                    Memory[location] = 0
                    Time += int(Parts[location]['time'])
                except:
                    pass
        if len(Queue16) != 0:
            data = Queue16.pop()
            location = where(data)
            if location == -1:
                Queue16.insert(0, data)
            else:
                try:
                    Memory[location] = 0
                    Time += int(Parts[location]['time'])
                except:
                    pass
        if len(Queue32) != 0:
            data = Queue32.pop()
            location = where(data)
            if location == -1:
                Queue32.insert(0, data)
            else:
                try:
                    Memory[location] = 0
                    Time += int(Parts[location]['time'])
                except:
                    pass
    else:
        if len(Queue) != 0:
            data = Queue.pop()
            location = where(data)
            if location == -1:
                Queue2.insert(0, data)
            else:
                try:
                    Memory[location] = 1
                    Time += int(Parts[location]['time'])
                except:
                    pass


def compute(processes, mq):
    global Parts, Queue, Queue2, Queue4, Queue8, Queue16, Queue32, Memory, Time, Count
    clear()
    Parts = processes.copy()

    if mq:
        for i in range(0, len(Parts)):
            if int(Parts[i]['memory']) == 2:
                Queue2.insert(0, i)
            elif int(Parts[i]['memory']) == 4:
                Queue4.insert(0, i)
            elif int(Parts[i]['memory']) == 8:
                Queue8.insert(0, i)
            elif int(Parts[i]['memory']) == 16:
                Queue16.insert(0, i)
            elif int(Parts[i]['memory']) == 32:
                Queue32.insert(0, i)

        for i in range(0, len(Parts)):
            enter(mq)
            Count += 1

        return Time / Count

    else:
        for i in range(0, len(Parts)):
            Queue.insert(0, i)

        for i in range(0, len(Parts)):
            enter(mq)
            Count += 1

        return Time / Count
