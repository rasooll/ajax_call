from django.shortcuts import render
from django.http import JsonResponse
import json
from .controlers import compute
from django.views.decorators.csrf import csrf_exempt


def index(request):
    return render(request, 'index.html')


@csrf_exempt
def calculate(request):
    data = None
    memory = 0
    time = 0
    if request.method == "POST":
        data = json.loads(request.body)
        data = [x for x in data if x is not None]

    for pro in data:
        memory += int(pro['memory'])
        time += int(pro['time'])

    result = {
        "count": len(data),
        "memory": memory,
        "time": time,
        "p1q": compute(data, False),
        "p5q": compute(data, True)
    }
    return JsonResponse(result)
