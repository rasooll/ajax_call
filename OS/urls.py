from django.urls import path
from .views import index, calculate
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('', index),
    path('calculate/', calculate)
]
urlpatterns += staticfiles_urlpatterns()
