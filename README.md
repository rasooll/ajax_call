# OS_Partitions
a simple django 2, ajax call project using pure JS, that calculate the performance of system memory (Theoretically!)

# Requirements
* python 3.6
* django 2

# Running
be careful to change the ```DEBUG = True``` for testing projext with local Django server.

then:

```
python3 manage.py runserver
```

and then checkout ```http://127.0.0.1:8000```
