var processes = [];
var id = 0;


function add_process() {
    var memory = parseInt(document.getElementById("memory").value);
    var time = parseInt(document.getElementById("time").value);
    processes.push({
        memory: memory,
        time: time
    });

    var new_process = document.createElement("div");
    new_process.id = "process_" + id;
    new_process.classList.add('row');
    new_process.classList.add('process');
    new_process.classList.add('no-gutters');
    new_process.classList.add('full-rounded');
    new_process.innerHTML = "                            <div class=\"col-5 text-center\">\n" +
        "                                <span class=\"\">Memory: " + memory + "</span>\n" +
        "                            </div>\n" +
        "                            <div class=\"col-5 text-center\">\n" +
        "                                <span class=\"\">Time: " + time + "</span>\n" +
        "                            </div>\n" +
        "                            <div class=\"col-2\">\n" +
        "                                <button class='btn-delete btn btn-sm btn-danger btn-full-width right-rounded'\n" +
        "                                        onclick=del_process(" + id + ")>Delete\n" +
        "                                </button>\n" +
        "                            </div>\n";
    var process_list = document.getElementById("process_list");
    process_list.appendChild(new_process);

    id += 1;
}


function del_process(id) {
    delete processes[id];
    var process_list = document.getElementById("process_list");
    var process = document.getElementById("process_" + id);
    process_list.removeChild(process);
}


function calculate() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "/calculate/");
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send(JSON.stringify(processes));
    xmlhttp.addEventListener('load', success);
}

function success() {
    var data = JSON.parse(this.responseText);
    // alert(data.done)
    var cal_result = document.createElement('div');
    cal_result.classList.add('process');
    cal_result.classList.add('no-gutters');
    cal_result.classList.add('full-rounded');
    cal_result.innerHTML = "<div class=\"row no-gutters\">\n" +
        "                                <div class=\"col-md-4 col-sm-12 text-center\">\n" +
        "                                    <span class=\"\">Processes No: " + data.count + "</span>\n" +
        "                                </div>\n" +
        "                                <div class=\"col-md-4 col-sm-12 text-center\">\n" +
        "                                    <span class=\"\">Total Memory: " + data.memory + "</span>\n" +
        "                                </div>\n" +
        "                                <div class=\"col-md-4 col-sm-12 text-center\">\n" +
        "                                    <span class=\"\">Total Time: " + data.time + "</span>\n" +
        "                                </div>\n" +
        "                            </div>\n" +
        "                            <div class=\"row no-gutters\">\n" +
        "                                <div class=\"col-md-6 col-sm-12 text-center\">\n" +
        "                                    <span class=\"\">Performance (1 Queue): " + data.p1q + "</span>\n" +
        "                                </div>\n" +
        "                                <div class=\"col-md-6 col-sm-12 text-center\">\n" +
        "                                    <span class=\"\">Performance (5 Queues): " + data.p5q + "</span>\n" +
        "                                </div>\n" +
        "                            </div>";
    var result_list = document.getElementById("result_list");
    result_list.appendChild(cal_result);
}